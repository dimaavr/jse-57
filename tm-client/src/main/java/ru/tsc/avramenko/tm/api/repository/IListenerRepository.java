package ru.tsc.avramenko.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.listener.AbstractListener;

import java.util.Collection;

public interface IListenerRepository {

    @NotNull
    Collection<AbstractListener> getCommands();

    @NotNull
    Collection<AbstractListener> getArguments();

    @NotNull
    AbstractListener getCommandByName(@Nullable String name);

    @NotNull
    AbstractListener getCommandByArg(@Nullable String arg);

    void add(@NotNull AbstractListener command);

}
