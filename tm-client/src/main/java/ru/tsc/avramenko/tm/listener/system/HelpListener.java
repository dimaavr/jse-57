package ru.tsc.avramenko.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.IListenerService;
import ru.tsc.avramenko.tm.listener.AbstractListener;
import ru.tsc.avramenko.tm.event.ConsoleEvent;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class HelpListener extends AbstractListener {

    @Autowired
    private IListenerService listenerService;

    @NotNull
    @Autowired
    private AbstractListener[] abstractListeners;

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @Nullable
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list of commands.";
    }

    @Override
    @EventListener(condition = "@helpListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[HELP]");
        int index = 1;
        final HashMap<String, String> cmd = new HashMap<>();
        for (final AbstractListener command : listenerService.getCommands()) {
            cmd.put(command.toString(), command
                    .getClass()
                    .getPackage()
                    .getName()
                    .substring(command.getClass().getPackage().getName().lastIndexOf(".")+1).toUpperCase());
        }
        final Map<String, List<String>> valueMap = cmd.keySet().stream().collect(Collectors.groupingBy(k -> cmd.get(k)));
        for(String s : valueMap.keySet()) {
            System.out.println("\n" + s + " COMMAND:");
            for (String str : valueMap.get(s)) {
                System.out.println(index + ". " + str);
                index++;
            }
        }
    }

}