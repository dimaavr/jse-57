package ru.tsc.avramenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.marker.SoapCategory;
import javax.xml.ws.WebServiceException;
import java.util.List;
import java.util.Random;

public class AdminUserEndpointTest {

    @NotNull
    private final static SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final static SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final static AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();

    @NotNull
    private final static AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    @NotNull
    private final static UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    private final static UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @NotNull
    private final static TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final static TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final static ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final static ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @Nullable
    private SessionDTO session;

    @NotNull
    private final Random r = new Random();

    @BeforeClass
    public static void beforeClass() {
        if (!userEndpoint.existsUserByLogin("Test")) {
            userEndpoint.registryUser("Test", "Test", "Test@email.com");
        }
    }

    @Before
    public void before() {
        session = sessionEndpoint.openSession("Admin", "Admin");
    }

    @After
    public void after() {
        taskEndpoint.clearTask(session);
        projectEndpoint.clearProject(session);
        sessionEndpoint.closeSession(session);
    }

    @Test
    @Category(SoapCategory.class)
    public void findAllUser() {
        @NotNull final List<UserDTO> list = adminUserEndpoint.findAllUser(session);
        Assert.assertNotNull(list);
    }

    @Test
    @Category(SoapCategory.class)
    public void findUserById() {
        @NotNull final UserDTO user = adminUserEndpoint.findUserById(session, session.getUserId());
        Assert.assertNotNull(user);
        Assert.assertEquals(session.getUserId(), user.getId());
    }

    @Test
    @Category(SoapCategory.class)
    public void findUserByLogin() {
        @NotNull final UserDTO user = adminUserEndpoint.findUserByLogin(session, "Admin");
        Assert.assertNotNull(user);
        Assert.assertEquals("Admin", user.getLogin());
    }

    @Test
    @Category(SoapCategory.class)
    public void lockUserByLogin() {
        adminUserEndpoint.lockUserByLogin(session, "User");
        @NotNull final UserDTO user = adminUserEndpoint.findUserByLogin(session, "User");
        Assert.assertNotNull(user);
        Assert.assertTrue(user.isLocked());
    }

    @Test
    @Category(SoapCategory.class)
    public void unlockByLogin() {
        adminUserEndpoint.unlockUserByLogin(session, "User");
        @NotNull final UserDTO user = adminUserEndpoint.findUserByLogin(session, "User");
        Assert.assertNotNull(user);
        Assert.assertFalse(user.isLocked());
    }

    @Test
    @Category(SoapCategory.class)
    public void updateUserById() {
        @NotNull final int numberEmail = r.nextInt(100);
        @NotNull final UserDTO user = adminUserEndpoint.updateUserById(session, "FirstNameNew", "LastNameNew", "MiddleNameNew", "EmailNew" + numberEmail);
        Assert.assertNotNull(user);
        Assert.assertEquals("FirstNameNew", user.getFirstName());
        Assert.assertEquals("LastNameNew", user.getLastName());
        Assert.assertEquals("MiddleNameNew", user.getMiddleName());
        Assert.assertEquals("EmailNew" + numberEmail, user.getEmail());
    }

    @Test
    @Category(SoapCategory.class)
    public void updateUserByLogin() {
        @NotNull final int numberEmail = r.nextInt(100);
        @NotNull final UserDTO user = adminUserEndpoint.updateUserByLogin(session, "Admin", "FirstNameNew2", "LastNameNew2", "MiddleNameNew2", "EmailNewTest" + numberEmail);
        Assert.assertNotNull(user);
        Assert.assertEquals("FirstNameNew2", user.getFirstName());
        Assert.assertEquals("LastNameNew2", user.getLastName());
        Assert.assertEquals("MiddleNameNew2", user.getMiddleName());
        Assert.assertEquals("EmailNewTest" + numberEmail, user.getEmail());
    }

    @Test
    @Category(SoapCategory.class)
    public void setUserRole() {
        @NotNull final UserDTO userTest = adminUserEndpoint.findUserByLogin(session, "User");
        @NotNull final UserDTO user = adminUserEndpoint.setUserRole(session, userTest.getId(), Role.ADMIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(Role.ADMIN, user.getRole());
        adminUserEndpoint.setUserRole(session, userTest.getId(), Role.USER);
    }

}