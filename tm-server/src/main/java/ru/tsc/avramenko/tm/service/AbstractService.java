package ru.tsc.avramenko.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public abstract class AbstractService {

    @Autowired
    protected ApplicationContext context;

}