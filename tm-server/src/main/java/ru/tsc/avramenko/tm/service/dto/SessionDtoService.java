package ru.tsc.avramenko.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.avramenko.tm.api.repository.dto.ISessionDtoRepository;
import ru.tsc.avramenko.tm.api.service.IPropertyService;
import ru.tsc.avramenko.tm.api.service.dto.ISessionDtoService;
import ru.tsc.avramenko.tm.api.service.dto.IUserDtoService;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import ru.tsc.avramenko.tm.dto.UserDTO;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.repository.dto.SessionDtoRepository;
import ru.tsc.avramenko.tm.service.AbstractService;
import ru.tsc.avramenko.tm.util.HashUtil;

import java.util.List;

@Service
public class SessionDtoService extends AbstractService implements ISessionDtoService {

    @Autowired
    private IUserDtoService userDtoService;

    @Autowired
    private IPropertyService propertyService;

    @Override
    @SneakyThrows
    public boolean checkDataAccess(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final UserDTO user = userDtoService.findByLogin(login);
        if (user == null) return false;
        if (user.getLocked()) return false;
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) return false;
        return hash.equals(user.getPasswordHash());
    }

    @Override
    @Nullable
    @SneakyThrows
    public SessionDTO sign(@Nullable final SessionDTO session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.sign(propertyService, session);
        session.setSignature(signature);
        return session;
    }


    @Override
    @SneakyThrows
    public void close(@NotNull SessionDTO session) {
        @NotNull ISessionDtoRepository sessionDtoRepository = context.getBean(SessionDtoRepository.class);
        try {
            sessionDtoRepository.getTransaction().begin();
            validate(session);
            sessionDtoRepository.remove(session);
            sessionDtoRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            sessionDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            sessionDtoRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public SessionDTO open(@Nullable String login, @Nullable String password) {
        boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        @Nullable final UserDTO user = userDtoService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        @NotNull ISessionDtoRepository sessionDtoRepository = context.getBean(SessionDtoRepository.class);
        try {
            sessionDtoRepository.getTransaction().begin();
            @Nullable final SessionDTO resultSession = sign(session);
            sessionDtoRepository.add(resultSession);
            sessionDtoRepository.getTransaction().commit();
            return resultSession;
        } catch (@NotNull final Exception e) {
            sessionDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            sessionDtoRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void validate(@NotNull SessionDTO session, @Nullable Role role) throws AccessDeniedException {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final UserDTO user = userDtoService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (user.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable SessionDTO session) throws AccessDeniedException {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final SessionDTO temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull ISessionDtoRepository sessionDtoRepository = context.getBean(SessionDtoRepository.class);
        try {
            if (sessionDtoRepository.findById(session.getId()) == null) throw new AccessDeniedException();
        } finally {
            sessionDtoRepository.close();
        }
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll() {
        @NotNull ISessionDtoRepository sessionDtoRepository = context.getBean(SessionDtoRepository.class);
        try {
            return sessionDtoRepository.findAll();
        } finally {
            sessionDtoRepository.close();
        }
    }

    @Nullable
    @Override
    public SessionDTO findById(@Nullable String id) {
        @NotNull ISessionDtoRepository sessionDtoRepository = context.getBean(SessionDtoRepository.class);
        try {
            return sessionDtoRepository.findById(id);
        } finally {
            sessionDtoRepository.close();
        }
    }

}