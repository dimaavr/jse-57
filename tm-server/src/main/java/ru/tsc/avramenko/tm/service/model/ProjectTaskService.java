package ru.tsc.avramenko.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.avramenko.tm.api.repository.model.IProjectRepository;
import ru.tsc.avramenko.tm.api.repository.model.ITaskRepository;
import ru.tsc.avramenko.tm.api.service.model.IProjectTaskService;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.exception.empty.EmptyIdException;
import ru.tsc.avramenko.tm.exception.empty.EmptyIndexException;
import ru.tsc.avramenko.tm.exception.empty.EmptyNameException;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.repository.model.ProjectRepository;
import ru.tsc.avramenko.tm.repository.model.TaskRepository;
import ru.tsc.avramenko.tm.service.AbstractService;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectTaskService extends AbstractService implements IProjectTaskService {

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            return taskRepository.findAllTaskByProjectId(userId, projectId);
        } finally {
            taskRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task bindTaskById(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        @NotNull final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            taskRepository.getTransaction().begin();
            @Nullable final Project project = projectRepository.findById(userId, projectId);
            @Nullable final Task task = taskRepository.findById(userId, taskId);
            task.setProject(project);
            taskRepository.update(task);
            taskRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task unbindTaskById(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            taskRepository.getTransaction().begin();
            @Nullable final Task task = taskRepository.findById(userId, taskId);
            task.setProject(null);
            taskRepository.update(task);
            taskRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        @NotNull final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            taskRepository.getTransaction().begin();
            projectRepository.getTransaction().begin();

            taskRepository.unbindAllTaskByProjectId(userId, projectId);
            projectRepository.removeById(userId, projectId);

            projectRepository.getTransaction().commit();
            taskRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            projectRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
            projectRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        @NotNull final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            taskRepository.getTransaction().begin();
            @NotNull final String projectId = Optional.ofNullable(projectRepository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new)
                    .getId();
            taskRepository.unbindAllTaskByProjectId(userId, projectId);
            projectRepository.removeByIndex(userId, index);
            taskRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        @NotNull final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            taskRepository.getTransaction().begin();
            projectRepository.getTransaction().begin();

            @NotNull final String projectId = Optional.ofNullable(projectRepository.findByName(userId, name))
                    .orElseThrow(ProjectNotFoundException::new)
                    .getId();

            taskRepository.unbindAllTaskByProjectId(userId, projectId);
            projectRepository.removeByName(userId, name);

            taskRepository.getTransaction().commit();
            projectRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            projectRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
            projectRepository.close();
        }
    }

}