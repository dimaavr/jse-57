# TASK MANAGER

SCREENSHOTS

https://yadi.sk/d/EQE6nSS9MMH3RA?w=1

## DEVELOPER INFO

name: Avramenko Dmitry

e-mail: DimaAvramenko@yandex.ru

e-mail: davramenko@tsconsulting.com

## HARDWARE

CPU: Intel(R) Pentium(R) Gold G5420 CPU

RAM: 8GB

HDD: 250GB

## SOFTWARE

System: Windows 10 Version 20H2

Version JDK: 1.8.0_282

## APPLICATION BUILD

```bash
mvn clean install
```

## APPLICATION RUN

```bash
java -jar ./task-manager.jar
```
